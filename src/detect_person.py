#!/usr/bin/python
import rospy
import sys
from sensor_msgs.msg import Image
from lasr_object_detection_yolo.srv import YoloDetection as yl

def detect_person():
    image_msg=rospy.wait_for_message('/xtion/rgb/image_raw', Image)
    rospy.wait_for_service('/yolo_detection')        
    objs = rospy.ServiceProxy('/yolo_detection', yl)
    res = objs(image_msg, 'coco', 0.5, 0.5)
    for o in res.detected_objects:
        if o.name == 'person':
            return '1'
    return None
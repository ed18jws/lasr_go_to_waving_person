#!/usr/bin/python
import rospy
import smach
import math
import actionlib
import sys
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Point, Pose, Quaternion, PointStamped, Vector3, PoseWithCovarianceStamped
from rotate import rotate
from get_closer_to_person import get_closer_to_person
from detect_person import detect_person
from location import getLocation

def get_euclidian(x1, y1, x2, y2):
    euclidian_distance = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)  
    return euclidian_distance

class World:
    def __init__(self):
        self.locations = []

world = World()
class InspectRoom(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome1','outcome2'])
    def execute(self, userdata):
        rospy.loginfo('Executing state InspectRoom')
        movebase_client = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
        movebase_client.wait_for_server()
        locations = rospy.get_param('/locations')  
        for location in locations:
            status = rospy.get_param('/locations/' + location + '/status')
            if status == 'unchecked':
                goal = MoveBaseGoal()
                goal.target_pose.header.stamp = rospy.Time.now()
                goal.target_pose.header.frame_id = 'map'
                goal.target_pose.pose = Pose(position = Point(**locations[location]['position']),
                                            orientation = Quaternion(**locations[location]['orientation']))
                movebase_client.send_goal(goal)
                rospy.loginfo('GOAL SENT! o: ' + location)
                # waits for the server to finish performing the action
                if movebase_client.wait_for_result():
                    rospy.loginfo('Goal point achieved!')
                    rospy.set_param('/locations/' + location + '/status', 'checked')
                else:
                    rospy.logwarn("Couldn't reach the goal!")
                return 'outcome1'
        return 'outcome2'

class DetectWavingPerson(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome1','outcome2'])
    def execute(self, userdata):
        rospy.loginfo('Executing state DetectWavingPerson')
        amcl_msg = rospy.wait_for_message('/amcl_pose', PoseWithCovarianceStamped)
        robot_pose = amcl_msg.pose.pose.position
        if detect_person() is None:
            return 'outcome2'
        loc = getLocation()
        if loc is None:
            return 'outcome2'
        #for locations in world.locations:
        #    pl_x = int(locations.point.x)
        #    pl_y = int(locations.point.y)
        #    nl_x = int(loc.point.x)
        #    nl_y = int(loc.point.y)
        #    if pl_x == nl_x and pl_y == nl_y:
        #        return 'outcome2'   
        euclidian = get_euclidian(robot_pose.x, robot_pose.y, loc.point.x, loc.point.y)
        if euclidian > 2:
            return 'outcome2'
        world.locations.append(loc)
        return 'outcome1'

        

class SpeakToPerson(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome1'])
    def execute(self, userdata):
        rospy.loginfo('Executing state SpeakToPerson')
        #Speech stuff
        return 'outcome1'

class GoToBar(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['outcome1'])
    def execute(self, userdata):
        rospy.loginfo('Executing state GoToBar')

        movebase_client = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
        movebase_client.wait_for_server()
        bar_location = rospy.get_param('/bar')
        goal = MoveBaseGoal()
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.pose = Pose(position = Point(**bar_location['position']),
                                    orientation = Quaternion(**bar_location['orientation']))
        movebase_client.send_goal(goal)

        rospy.loginfo('GOAL SENT! o:')
        if movebase_client.wait_for_result():
            rospy.loginfo('Goal location achieved!')
        else:
            rospy.logwarn("Couldn't reach goal")
    
        return 'outcome1'

def main():
    rospy.init_node('cleanup_state_machine')
    #State machine
    sm = smach.StateMachine(outcomes=['outcome1', 'end'])
    with sm:
        smach.StateMachine.add('InspectRoom', InspectRoom(), transitions={'outcome1': 'DetectWavingPerson', 'outcome2' : 'end'})
        smach.StateMachine.add('DetectWavingPerson', DetectWavingPerson(), transitions={'outcome1':'SpeakToPerson','outcome2':'InspectRoom'})
        smach.StateMachine.add('SpeakToPerson', SpeakToPerson(), transitions={'outcome1':'GoToBar'})
        smach.StateMachine.add('GoToBar', GoToBar(), transitions={'outcome1': 'InspectRoom'})
    outcome = sm.execute()



if __name__=='__main__':
    try:
        main()
    #wait for keyboard interrupt
    except rospy.ROSInterruptException:
        rospy.loginfo('State Machine terminated.')
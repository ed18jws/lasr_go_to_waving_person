#!/usr/bin/python
import rospy
import sys
import cv2
from std_msgs.msg import String
from std_msgs.msg import Float32
from sensor_msgs.msg import Image
from sensor_msgs.msg import PointCloud2, PointCloud
from cv_bridge import CvBridge, CvBridgeError
from lasr_object_detection_yolo.srv import YoloDetection as yl
import numpy
import tf
from geometry_msgs.msg import Point, PointStamped
import warnings



def getLocation():
    #location_pub = rospy.Publisher('person_location', PointStamped, queue_size = 10)
    br = CvBridge()
    while True:
        image_msg = rospy.wait_for_message('/xtion/rgb/image_raw', Image)
        trform = tf.TransformListener()
        rospy.wait_for_service('/yolo_detection')
        try:
            objs = rospy.ServiceProxy('/yolo_detection', yl)
            res = objs(image_msg, 'coco', 0.5, 0.3)
            for o in res.detected_objects:
                if o.name == 'person':
                    try:
                        # print('got person')
                        cv2_image = br.imgmsg_to_cv2(res.image_bb, "bgr8")
                        pcl2 = rospy.wait_for_message('xtion/depth_registered/points', PointCloud2)
                        trform.waitForTransform('xtion_rgb_optical_frame', 'map', pcl2.header.stamp, rospy.Duration(2.0))
                        #print('cloud')
                        cloud = numpy.fromstring(pcl2.data, numpy.float32)                      
                        c_x = int(o.xywh[0] + o.xywh[2]/2)
                        c_y = int(o.xywh[1] + o.xywh[3]/2)
                        c_y = c_y*pcl2.width*8
                        c_x = c_x*8
                        c_index = c_y + c_x
                        (x,y,z) = cloud[c_index : c_index +3 ]
                        p = Point(x, y, z)
                        loc = PointStamped()
                        loc.header = pcl2.header
                        loc.point = p
                        person_loc = trform.transformPoint('map', loc)
                        # print person_loc
                        return person_loc
                        # str_x = 'X: ' + str(round(person_loc.point.x,8))
                        # str_y = 'Y: ' + str(round(person_loc.point.y,8))
                        # str_z = 'Z: ' + str(round(person_loc.point.z,8))
                        # str_pub = str_x + ' ' + str_y + ' ' + str_z
                        #location_pub.publish(loc)
                        # print('loc')
                        #print(person_loc)
                    except CvBridgeError as e:
                        print(e)
        except rospy.ServiceException as e:
            print(e)
        return None                    
def main(args):
    rospy.init_node('location_getter', anonymous=True)
    # print('LOADED')
    getLocation()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down...")

if __name__ == '__main__':
    main(sys.argv)

